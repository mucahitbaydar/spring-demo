package com.universe.springdemo;

public class FootballCoach implements Coach{
	
	private FortuneService fortuneService;
	private String emailAddress;
	private String team;
	
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public void setTeam(String team) {
		this.team = team;
	}
	
	public void setFortuneService(FortuneService fortuneService) {
		System.out.println("FootballCoach: setter method called");
		this.fortuneService = fortuneService;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getTeam() {
		return team;
	}

	public FootballCoach() {
		System.out.println("FootballCoach: no-arg constructor called");
	}
	
	@Override
	public String getDailyWorkout() {
		return "Run forest Run";
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}



}
