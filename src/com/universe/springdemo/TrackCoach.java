package com.universe.springdemo;

public class TrackCoach implements Coach {

	private FortuneService fortuneService;
	
	public TrackCoach(){
		
	}
	
	public TrackCoach(FortuneService theFortuneService) {
		fortuneService = theFortuneService;
	}
	
	@Override
	public String getDailyWorkout() {
		return "Run a hard 5k";
	}

	@Override
	public String getDailyFortune() {
		return "Just Do It : " + fortuneService.getFortune();
	}
	
	public void doStartupStuff() {
		System.out.println("TrackCoach: inside init method");
	}
	
	public void doCleanupStuff() {
		System.out.println("TrackCoach: inside destroy method");
	}

}
